package streams

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import Bloxorz._

@RunWith(classOf[JUnitRunner])
class BloxorzSuite extends FunSuite {

  trait SolutionChecker extends GameDef with Solver with StringParserTerrain {
    /**
     * This method applies a list of moves `ls` to the block at position
     * `startPos`. This can be used to verify if a certain list of moves
     * is a valid solution, i.e. leads to the goal.
     */
    def solve(ls: List[Move]): Block =
      ls.foldLeft(startBlock) { case (block, move) => move match {
        case Left => block.left
        case Right => block.right
        case Up => block.up
        case Down => block.down
      }
    }
  }

  trait Level1 extends SolutionChecker {
      /* terrain for level 1*/

    val level =
    """ooo-------
      |oSoooo----
      |ooooooooo-
      |-ooooooooo
      |-----ooToo
      |------ooo-""".stripMargin

    val optsolution = List(Right, Right, Down, Right, Right, Right, Down)
  }

  trait NoSolutionLevel extends SolutionChecker {
      /* terrain for no-solution level*/

    val level =
    """ooo-------
      |oSoo-o----
      |ooo--oooo-
      |-oo-oooooo
      |-----ooToo
      |------ooo-""".stripMargin
  }

  test("terrain function level 1") {
    new Level1 {
      assert(terrain(Pos(0,0)), "0,0")
      assert(!terrain(Pos(4,11)), "4,11")
      assert(!terrain(Pos(7,0)), "7,0")
      assert(!terrain(Pos(0,10)), "0,10")
      assert(!terrain(Pos(0,3)), "0,3")
      assert(terrain(Pos(1,1)), "S")
      assert(terrain(Pos(4,7)), "T")
      assert(terrain(Pos(3,1)), "3,1")
      assert(terrain(Pos(5,8)), "5,8")
      assert(!terrain(Pos(5,9)), "5,9")
      assert(!terrain(Pos(5,10)), "5,10")
      assert(!terrain(Pos(6,8)), "6,8")
    }
  }

  test("findChar level 1") {
    new Level1 {
      assert(startPos === Pos(1,1))
      assert(goal === Pos(4,7))
      intercept[IllegalArgumentException] {
        findChar('V', toVector(level))
      }
    }
  }

  test("isStanding level 1") {
    new Level1 {
      assert(Block(Pos(2, 2), Pos(2, 2)).isStanding)
      assert(!Block(Pos(2, 1), Pos(2, 2)).isStanding)
      assert(!Block(Pos(1, 2), Pos(2, 2)).isStanding)
    }
  }

  test("isLegal level 1") {
    new Level1 {
      assert(!Block(Pos(0, 4), Pos(0, 5)).isLegal)
      assert(Block(Pos(2, 2), Pos(2, 2)).isLegal)
      assert(Block(Pos(4, 5), Pos(4, 6)).isLegal)
      assert(!Block(Pos(100, 100), Pos(100, 101)).isLegal)
      assert(!Block(Pos(-2, -2), Pos(-2, -1)).isLegal)
      assert(!Block(Pos(4, 0), Pos(5, 0)).isLegal)
    }
  }

  test("startBlock level 1") {
    new Level1 {
      assert(startBlock.isLegal)
      assert(startBlock.isStanding)
      assert(startBlock.b1 === startPos)
    }
  }

  test("neighbors level 1") {
    new Level1 {
      assert(startBlock.neighbors ===
             List((Block(Pos(1, -1), Pos(1, 0)), Left),
                  (Block(Pos(1, 2), Pos(1, 3)), Right),
                  (Block(Pos(-1, 1), Pos(0, 1)), Up),
                  (Block(Pos(2, 1), Pos(3, 1)), Down)))
      assert(Block(Pos(2, 2), Pos(2, 3)).neighbors ===
             List((Block(Pos(2, 1), Pos(2, 1)), Left),
                  (Block(Pos(2, 4), Pos(2, 4)), Right),
                  (Block(Pos(1, 2), Pos(1, 3)), Up),
                  (Block(Pos(3, 2), Pos(3, 3)), Down)))
      assert(Block(Pos(3, 5), Pos(4, 5)).neighbors ===
             List((Block(Pos(3, 4), Pos(4, 4)), Left),
                  (Block(Pos(3, 6), Pos(4, 6)), Right),
                  (Block(Pos(2, 5), Pos(2, 5)), Up),
                  (Block(Pos(5, 5), Pos(5, 5)), Down)))
    }
  }

  test("legalNeighbors level 1") {
    new Level1 {
      assert(startBlock.legalNeighbors ===
             List((Block(Pos(1, 2), Pos(1, 3)), Right),
                  (Block(Pos(2, 1), Pos(3, 1)), Down)))
      assert(Block(Pos(3, 4), Pos(3, 4)).legalNeighbors ===
             List((Block(Pos(3, 2), Pos(3, 3)), Left),
                  (Block(Pos(3, 5), Pos(3, 6)), Right),
                  (Block(Pos(1, 4), Pos(2, 4)), Up)))
      assert(Block(Pos(2, 2), Pos(2, 3)).legalNeighbors === Block(Pos(2, 2), Pos(2, 3)).neighbors)
      assert(Block(Pos(3, 5), Pos(4, 5)).legalNeighbors ===
             List((Block(Pos(3, 6), Pos(4, 6)), Right),
                  (Block(Pos(2, 5), Pos(2, 5)), Up)))
    }
  }

  test("done level 1") {
    new Level1 {
      assert(done(Block(Pos(4, 7), Pos(4, 7))))
      assert(!done(Block(Pos(4, 6), Pos(4, 7))))
      assert(!done(Block(Pos(4, 7), Pos(4, 8))))
      assert(!done(Block(Pos(3, 7), Pos(4, 7))))
      assert(!done(Block(Pos(4, 7), Pos(5, 7))))
      assert(!done(Block(Pos(1, 1), Pos(1, 1))))
    }
  }

  test("neighborsWithHistory level 1") {
    new Level1 {
      assert(neighborsWithHistory(startBlock, Nil) ===
             Stream((Block(Pos(1, 2), Pos(1, 3)), List(Right)),
                    (Block(Pos(2, 1), Pos(3, 1)), List(Down))))
      assert(neighborsWithHistory(Block(Pos(1, 2), Pos(1, 3)), List(Right)) ===
             Stream((startBlock, List(Left, Right)),
                    (Block(Pos(1, 4), Pos(1, 4)), List(Right, Right)),
                    (Block(Pos(2, 2), Pos(2, 3)), List(Down, Right))))
      assert(neighborsWithHistory(Block(Pos(2, 1), Pos(3, 1)), List(Down)) ===
             Stream((Block(Pos(2, 2), Pos(3, 2)), List(Right, Down)),
                    (startBlock, List(Up, Down))))
      assert(neighborsWithHistory(startBlock, List(Left, Up)) ===
             Stream((Block(Pos(1, 2), Pos(1, 3)), List(Right, Left, Up)),
                    (Block(Pos(2, 1), Pos(3, 1)), List(Down, Left, Up))))
    }
  }

  test("newNeighborsOnly level 1") {
    new Level1 {
      val startingNeighbors = neighborsWithHistory(startBlock, Nil)
      assert(newNeighborsOnly(Stream.empty, Set.empty) === Stream.empty)
      assert(newNeighborsOnly(Stream((startBlock, Nil)), Set(startBlock)) === Stream.empty)
      assert(newNeighborsOnly(startingNeighbors, Set.empty) === startingNeighbors)
      assert(newNeighborsOnly(Stream((Block(Pos(1, 2), Pos(1, 3)), List(Right, Left, Up)),
                                     (Block(Pos(2, 1), Pos(3, 1)), List(Down, Left, Up))),
                              Set(Block(Pos(1, 2), Pos(1, 3)), startBlock)) ===
             Stream((Block(Pos(2, 1), Pos(3, 1)), List(Down, Left, Up))))
    }
  }

  test("from level 1") {
    new Level1 {
      assert(from(Stream.empty, Set.empty) === Stream.empty)
      assert(from(Stream((startBlock, Nil)), Set(startBlock)).head._2.size < 7)
    }
  }

  test("optimal solution for level 1") {
    new Level1 {
      assert(solve(solution) === Block(goal, goal))
    }
  }

  test("optimal solution length for level 1") {
    new Level1 {
      assert(solution.length === optsolution.length)
    }
  }

  test("no solution possible") {
    new NoSolutionLevel {
      assert(solution.isEmpty)
    }
  }
}
